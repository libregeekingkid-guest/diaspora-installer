#! /bin/sh

# Read configuration from file
. /etc/diaspora/diaspora-common.conf
if test -f ${diaspora_conf}; then
  echo "Using ${diaspora_conf}..."
  
  if ! grep BUNDLE_WITH ${diaspora_conf} >/dev/null; then
    echo "Older versions did not set BUNDLE_WITH variable"
    echo "Setting BUNDLE_WITH variable in ${diaspora_conf}..."
  
  if [ $(grep DB ${diaspora_conf} |cut -d= -f2) = "postgres" ]; then
      echo export BUNDLE_WITH=postgresql >> ${diaspora_conf}
    else
      echo export BUNDLE_WITH=mysql >> ${diaspora_conf}
    fi
  fi
    
  if ! grep DB_NAME ${diaspora_conf} ; then
    echo "Older versions did not set DB_NAME variable"
    echo "Setting DB_NAME variable in ${diaspora_conf}..."
    echo export DB_NAME=diaspora_production >> ${diaspora_conf}
  fi

  # source diaspora variables	
  . ${diaspora_conf}
else
  echo "Using ${diaspora_conf_private}..."
  if ! grep RAILS_ENV ${diaspora_conf_private}; then
    echo export RAILS_ENV=production >> ${diaspora_conf_private}
  fi

  if ! grep DB ${diaspora_conf_private}; then
    echo export DB=postgres >> ${diaspora_conf_private}
  fi
	
  if ! grep BUNDLE_WITH ${diaspora_conf_private}; then
    echo export BUNDLE_WITH=postgresql >> ${diaspora_conf_private}
  fi
	
  # source diaspora variables	
  . ${diaspora_conf_private}
fi
